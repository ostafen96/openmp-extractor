# OpenMPExtractor: collect OpenMP pragma code fragments

OpenMPExtractor permette di estrarre gli snippet di codice C/C++ marcati da un pragma OpenMP e di collezionarli in un singolo file in formato csv.

# Requisiti
  - Java >= 8
  - Apache Commons CLI Library
  - Eclipse CDT

Le dipendenze elencate sopra dovranno essere include dentro una cartella "jars" al momento della compilazione.

# Esempio di esecuzione

Se il file OpenMPExtractor.jar è stato compilato correttamente, sarà possibile mostrare un semplice usage eseguendolo:

```sh
$ java -jar OpenMPExtractor.jar
usage: OpenMPExtractor [-h] [-l] [-o <out-name.csv>] -p <PATH>
Extract and collect OpenMP pragma code fragments.

 -h,--help                    Print usage
 -l,--loops-only              Extract only loop fragments
 -o,--output <out-name.csv>   Output csv filename
 -p,--Path <PATH>             File or path to process

Author: Stefano Scafiti
```