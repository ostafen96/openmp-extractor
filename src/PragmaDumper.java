import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class PragmaDumper {
    private static final String CODE_FIELD = "code";
    private static final String PRAGMA_FIELD = "pragma";

    public static void dump(String filename, List<Pragma> pragmas) throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(Paths.get(filename));
        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(CODE_FIELD, PRAGMA_FIELD));

        for(Pragma p : pragmas) {
            String pragmaString = p.getPragmaString();
            String code = p.getBody();
            csvPrinter.printRecord(code, pragmaString);
        }
        csvPrinter.flush();
    }
}
