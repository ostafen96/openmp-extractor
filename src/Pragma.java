public class Pragma {
    private String pragmaString;
    private String pragmaBody;
    private int startLine, endLine;
    private boolean isLoopStmt = false;

    public Pragma(String pragma, int startLine, int endLine) {
        this.pragmaString = pragma;
        this.startLine = startLine;
        this.endLine = endLine;
    }

    public void setIsLoopStmt(boolean isLoopStmt) {
        this.isLoopStmt = isLoopStmt;
    }

    public boolean isLoopStmt() {
        return isLoopStmt;
    }

    public void setBody(String body) {
        this.pragmaBody = body;
    }

    public boolean isEmpty() {
        return pragmaBody == null;
    }

    public int startLine() {
        return startLine;
    }

    public int endLine() {
        return endLine;
    }

    @Override
    public String toString() {
        return pragmaString + (pragmaBody == null ? "\n" : ("\n" + pragmaBody));
    }

    public String getBody() {
        return pragmaBody;
    }

    public String getPragmaString() {
        return pragmaString;
    }
}
