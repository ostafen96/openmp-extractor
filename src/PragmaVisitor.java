import org.eclipse.cdt.core.dom.ast.*;
import org.eclipse.cdt.internal.core.dom.rewrite.astwriter.ASTWriter;

import javax.swing.table.TableRowSorter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class PragmaVisitor extends ASTVisitor {
    private List<Pragma> pragmas;
    private ASTWriter writer = new ASTWriter();
    private int index = 0;

    public PragmaVisitor(IASTTranslationUnit translationUnit) {
        this.pragmas = getAllPragmas(translationUnit);
        this.shouldVisitStatements = true;
    }

    public int visit(IASTStatement stmt) {

        if (index < pragmas.size()) {
            int stmtStartLine = stmt.getFileLocation().getStartingLineNumber();

            Pragma currentPragma = pragmas.get(index);
            //int pragmaStartLine = currentPragma.startLine();
            int pragmaEndLine = currentPragma.endLine();

            boolean pragmaFound = true;
            if (stmtStartLine > pragmaEndLine) {

                boolean anyPragma = pragmas.stream().anyMatch(p -> p.startLine() > pragmaEndLine
                        && p.endLine() < stmtStartLine);

                if (anyPragma) {
                    //continue until there are empty pragmas
                    index++;
                    while (anyPragma && index < pragmas.size()) {
                        Pragma nextPragma = pragmas.get(index);

                        anyPragma = nextPragma.startLine() > currentPragma.endLine()
                                && nextPragma.endLine() < stmtStartLine;

                        currentPragma = nextPragma;
                        index = index + 1;
                    }

                    if (index >= pragmas.size())
                        pragmaFound = false;
                }

                if (pragmaFound) {
                    String stmtCode = writer.write(stmt);
                    currentPragma.setBody(stmtCode);
                    currentPragma.setIsLoopStmt(stmt instanceof IASTForStatement);
                }
                index++;
            }
        }
        return 3;
    }

    public List<Pragma> results() {
        return pragmas.stream()
                .filter(p -> !p.isEmpty())
                .collect(Collectors.toList());
    }

    public List<Pragma> getAllPragmas(IASTTranslationUnit translationUnit) {
        List<Pragma> pragmas = new ArrayList<>();
        for(IASTPreprocessorStatement stmt : translationUnit.getAllPreprocessorStatements()) {
            if(stmt instanceof IASTPreprocessorPragmaStatement) {
                IASTPreprocessorPragmaStatement pragmaStmt = (IASTPreprocessorPragmaStatement) stmt;
                int startingLine = pragmaStmt.getFileLocation().getStartingLineNumber();
                int endingLine = pragmaStmt.getFileLocation().getEndingLineNumber();
                Pragma p = new Pragma(pragmaStmt.toString(), startingLine, endingLine);
                pragmas.add(p);
            }
        }
        return pragmas;
    }
}
