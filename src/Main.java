import org.apache.commons.cli.*;
import org.eclipse.cdt.core.dom.ast.*;
import org.eclipse.cdt.core.dom.ast.gnu.cpp.GPPLanguage;
import org.eclipse.cdt.core.parser.*;
import org.eclipse.core.runtime.CoreException;

import javax.swing.plaf.metal.MetalBorders;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    private static final String C_EXT = ".c";
    private static final String CPP_EXT = ".cpp";
    private static boolean loopsOnly = false;

    public static void main(String[] args) throws IOException {
        Options options = new Options();

        options.addOption(Option.builder("p")
                .longOpt("Path")
                .desc("File or path to process.")
                .hasArg()
                .argName("PATH")
                .required()
                .build());

        options.addOption(Option.builder("o")
                .longOpt("output")
                .desc("Output csv filename")
                .hasArg()
                .argName("out-name.csv")
                .build());

        options.addOption("l", "loops-only", false, "Extract only loop fragments");
        options.addOption("h", "help", false, "Print usage");

        if(args.length > 0) {
            CommandLineParser parser = new DefaultParser();

            CommandLine cmd = null;
            try {
                cmd = parser.parse(options, args);
            } catch (ParseException e) {
                printUsageAndExit(options);
            }

            loopsOnly = cmd.hasOption('l');
            Stream<Path> paths = Files.walk(Paths.get(cmd.getOptionValue('p')));
            Optional<List<Pragma>> res = paths.
                    filter(p -> p.toString().endsWith(CPP_EXT) || p.toString().endsWith(C_EXT))
                    .map(Main::processFile)
                    .reduce((acc, l) -> {
                        List<Pragma> addList = new ArrayList<>(acc);
                        addList.addAll(l);
                        return addList;
                    });

            if (res.isPresent()) {
                List<Pragma> pragmaList = res.get();

                if(loopsOnly) {
                    pragmaList = pragmaList.stream()
                            .filter(p -> p.isLoopStmt())
                            .collect(Collectors.toList());
                }

                if(!pragmaList.isEmpty()) {
                    String outputName = cmd.hasOption('o') ? cmd.getOptionValue('o') : args[0] + ".csv";
                    PragmaDumper.dump(outputName, pragmaList);
                } else {
                    noPragmaWarning();
                }
            } else {
                noPragmaWarning();
            }
        } else {
            printUsageAndExit(options);
        }
    }

    private static void printUsageAndExit(Options opts) {
        String header = "Extract and collect OpenMP pragma code fragments.\n\n";
        String footer = "\nAuthor: Stefano Scafiti";
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("OpenMPExtractor", header, opts, footer, true);
        System.exit(1);
    }

    private static void noPragmaWarning() {
        System.out.println("Warning: no pragma extracted.\n");
        System.exit(0);
    }

    private static List<Pragma> processFile(Path path) {
        String filename = path.toString();

        FileContent fileContent = FileContent.createForExternalFileLocation(filename);
        Map definedSymbols = new HashMap();
        String[] includePaths = new String[0];
        IScannerInfo info = new ScannerInfo(definedSymbols, includePaths);
        IParserLogService log = new DefaultLogService();
        IncludeFileContentProvider emptyIncludes = IncludeFileContentProvider.getEmptyFilesProvider();

        int opts = 8;
        IASTTranslationUnit translationUnit = null;
        try {
            translationUnit = GPPLanguage.getDefault()
                    .getASTTranslationUnit(fileContent, info, emptyIncludes, null, opts, log);
        } catch (CoreException e) {
            e.printStackTrace();
        }

        PragmaVisitor pragmaVisitor = new PragmaVisitor(translationUnit);
        translationUnit.accept(pragmaVisitor);

        return pragmaVisitor.results();
    }
}